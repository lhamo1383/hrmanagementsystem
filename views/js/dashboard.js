import { showAlert } from "./alert.js"

// logging out

const logout = async () =>{
    try {
        const res = await axios({
            method: 'GET',
            url: 'http://localhost:4001/api/v1/users/logout',
        })
        if (res.data.status === 'success'){
            location.reload(true)
        }
    }   catch (err) {
        showAlert('error', 'Error logging out ! Try again')
    }
}
var obj 
if (document.cookie) {
    obj = JSON.parse(document.cookie.substring(6))
} else {
    obj = JSON.parse('{}')
}

var el = document.querySelector('.nav.nav--user')
if (obj._id) {
    el.innerHTML =
        '<a id ="logout" class="nav__el">Log out </a><a class="nav__el nav__el--cta" href="/add-employee">Add </a>'
    var doc = document.querySelector('#logout')

    doc.addEventListener('click', (e)=> logout())
}   else{
    el.innerHTML =
    '<a class="nav__el nav__el--cta" href="/login">Log in </a> '
}


const displayEmployees = async (table) => {
    try {
      const response = await axios.get('http://localhost:4001/api/v1/users');
      const Employees = response.data;
    const employees = Employees.data;
      console.log(employees)
      let row = '<tr>'
      for (let employee of employees) {
        if(employee.role === 'employee'){
            row += '<tr>';
            row += `
          <td>${employee.name}</td>
          <td>${employee.employeeid}</td>
          <td>${employee.gender}</td>
          <td>${employee.dob}</td>
          <td>${employee.designation}</td>
          <td>${employee.department}</td>
          <td>${employee.appointmentdate}</td>
          <td>
          <a class="edit-button" href="./edit-employee.html?id=${employee._id}">Edit</a>
          <button id="deleteButton-${employee._id}" onclick="deleteEmployee('${employee._id}')">Delete</button>

          </td>
        `;
        row += '</tr>'

        }
        
      };
      table.innerHTML = row
      console.log(row)
    } catch (error) {
      console.error(error);
    }
  };
  

const table = document.getElementById('data')

if (table) 
displayEmployees(table)

// Edit and Delete button event listeners
window.deleteEmployee = async (employeeId) => {
  try {
    // Send a delete request to the API
    await axios.delete(`http://localhost:4001/api/v1/users/${employeeId}`);
    
    // Refresh the employee list after successful deletion
    location.reload(500)
  } catch (error) {
    console.error(error);
  }
};
$(document).ready(function () {
    $('#example').DataTable();
});

