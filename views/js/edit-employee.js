// edit.js

const urlParams = new URLSearchParams(window.location.search);
const employeeId = urlParams.get('id');

const editForm = document.getElementById('editForm');
const nameInput = document.getElementById('name');
const idInput = document.getElementById('employeeid');
const fgenderInput = document.getElementById('female');
const mgenderInput = document.getElementById('female');
const dobInput = document.getElementById('dob');
const designationInput = document.getElementById('designation');
const departmentInput = document.getElementById('department');
const appointmentInput = document.getElementById('appointment');

// Fetch the employee details and populate the form fields
const fetchEmployeeDetails = async () => {
  try {
    const response = await axios.get(`http://localhost:4001/api/v1/users/${employeeId}`);
    const employee = response.data.data;
    console.log(employee)
    // Populate the form fields with the retrieved employee details
    nameInput.value = employee.name;
    if (employee.gender == 'female') {
        fgenderInput.checked = true
    } else {
        mgenderInput.checked = true
    }
    dobInput.value = employee.dob;
    designationInput.value = employee.designation;
    departmentInput.value = employee.department;
    idInput.value = employee.employeeid;
    appointmentInput.value = employee.appointmentdate;

    // Populate more form fields with other employee details

  } catch (error) {
    console.error(error);
  }
};

// Save the edited employee details
const saveEmployeeDetails = async () => {
  try {
    const gender = document.querySelector('input[name=gender]:checked').value

    const updatedEmployee = {
      name: nameInput.value, 
    gender: gender,
    dob:dobInput.value,
    designation:designationInput.value,
    department:departmentInput.value,
    employeeid:idInput.value,
    appointmentdate:appointmentInput.value
      
      

      // Include more updated fields here
    };

    await axios.put(`http://localhost:4001/api/v1/users/${employeeId}`, updatedEmployee);

    alert('Employee details updated successfully');

    // Redirect to the employee list page or perform other actions as needed
    window.location.href = './dashboard.html';
  } catch (error) {
    console.error(error);
  }
};

editForm.addEventListener('submit', (event) => {
  event.preventDefault();
  saveEmployeeDetails();
});

fetchEmployeeDetails();
