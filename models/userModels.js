const mongoose = require('mongoose')
const validator = require ('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose. Schema({
    name: {type: String, 
    required:[ true,'Please tell us your name!'],
    },
    email: {
        type: String,
        unique: true, 
        lowercase: true, 
        validate: [validator.isEmail ,'Please provide a valid email'],
    },
    password:{
        type: String, 

        //password wont be included when we get the users select: false,
    },  
    passwordConfirm: {
        type: String,
      
    },
    employeeid:{
        type:String
    },
    gender:{
        type:String
    },
    designation:{
        type:String
    },
    dob:{
        type:String
    },
    department:{
        type:String
    },
    appointmentdate:{
        type:String
    },
    role:{
        type:String
    },
    active: {
        type: Boolean, 
        default: true, 
        select: false,
    }
    
})
userSchema.pre('save', async function (next) {
    if(!this.isModified('password')) return next()

    this.password = await bcrypt.hash(this.password,12)

    this.passwordConfirm = undefined
    next()
})
userSchema.methods.correctPassword = async function (
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword, userPassword)
}
const User = mongoose.model ('User', userSchema)
module.exports = User