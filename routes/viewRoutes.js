const express = require('express')
const router = express.Router()
const viewController = require('./../controllers/viewController')
const authController = require('./../controllers/authController')

router.get('/',viewController.getHome)
router.get('/login', viewController.getLoginForm)
router.get('/signup', viewController.getSignupForm)
router.get('/me', authController.protect, viewController.getProfile)
router.get('/add-employee', authController.protect, viewController.getAddEmployee)
router.get('/edit-employee', authController.protect, viewController.geteditEmployee)

module.exports = router